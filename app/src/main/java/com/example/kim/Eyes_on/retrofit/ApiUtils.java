package com.example.kim.Eyes_on.retrofit;

import com.example.kim.Eyes_on.apiInterfaces.EyesApiInterface;
import com.example.kim.Eyes_on.apiInterfaces.UsersApiInterface;

/**
 * Created by Sangwoo Kim on 2017. 11. 26..
 */

public class ApiUtils {
    private ApiUtils() {}

    //public static final String BASE_URL = "https://my-json-server.typicode.com/";
    public static final String BASE_URL = "http://ec2-34-208-186-166.us-west-2.compute.amazonaws.com:8080";
    //public static final String BASE_URL = "https://jsonplaceholder.typicode.com";

    public static EyesApiInterface getEyesAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(EyesApiInterface.class);
    }

    public static UsersApiInterface getUsersAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(UsersApiInterface.class);
    }
}
