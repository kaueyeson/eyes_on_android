package com.example.kim.Eyes_on.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.MyMarkerView;
import com.example.kim.Eyes_on.activities.GraphActivity;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.example.kim.Eyes_on.R.id.chart2;

/**
 * Created by 김수봉 on 2017-05-18.
 */

public class SoilFragment extends Fragment{
    LineChart lineChart;
    public SoilFragment() {
        // Required empty public constructor
    }
    public class MyYAxisValueFormatter implements YAxisValueFormatter {

        private DecimalFormat mFormat;

        public MyYAxisValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }

        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return mFormat.format(value);// + "%";
        }
    };
    public class MyCustomXAxisValueFormatter implements XAxisValueFormatter {

        @Override
        public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
            // original is the original value to use, x-index is the index in your x-values array
            // implement your logic here ...
            viewPortHandler.hasNoDragOffset();
            if(viewPortHandler.getScaleX() > 2.2){
            }
            return original;
        }
    };
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GraphActivity repo = new GraphActivity();
        Context context = getActivity();
        MyMarkerView mv = new MyMarkerView(context,R.layout.content_marker_view);
        ArrayList<String> soil = repo.getSoil();
        ArrayList<String> time = repo.getTimeList();
        ArrayList<String> waterDetection = repo.getWaterDetection(); //임시로 만듦

        lineChart = (LineChart) getActivity().findViewById(chart2);
        XAxis xAxis = lineChart.getXAxis();

        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        ArrayList<Entry> yAXES2 = new ArrayList<Entry>();
        LineDataSet lineDataSet2 = new LineDataSet(yAXES2, "토양수분");
        int count = time.size() - 1;//int count = 0;
        int graph = 0;
        int b = 0;
        int xCount = 0;
        int size = 1;
        String[] XstrArr, strArr, Xvalue, value, secXvalue, secValue;
        int X = 0, a = 0;
        Log.d("timesize : ",String.valueOf(time.size()));
        if(time.size() != 0)
        {
            size = time.size();
            XstrArr = time.get(0).split("T");//XstrArr = time.get(size - 1).split("T"); //time.get(0)이 정상
            strArr = time.get(count).split("T");
            Xvalue = XstrArr[1].split("\\:");
            value = strArr[1].split("\\:");
            secValue = value[2].split("\\.");
            secXvalue = Xvalue[2].split("\\.");
            X = Integer.parseInt(Xvalue[0]) * 3600 + Integer.parseInt(Xvalue[1])* 60 + Integer.parseInt(secXvalue[0]) + (540*60); //초단위
            a = Integer.parseInt(value[0]) * 3600 + Integer.parseInt(value[1])*60 + Integer.parseInt(secValue[0]) + (540*60); //초단위
            if(X >= 86400)
            {
                X -= 86400;
            }
            if(a >= 86400)
            {
                a -= 86400;
            }
        }
        String[] xaxes = new String[X/10]; //10초단위

        Log.d("time.size",String.valueOf(time.size()));
        if(time.size() == 0)
        {
            Log.d("데이터값 없음",String.valueOf(time.size()));
            graph = 1;
        }
        else {
            Log.d("X =",String.valueOf(X));
            Log.d("a = ", String.valueOf(a));
            xaxes[0] = "0시";
            for (int i = 1; i < X / 10; i++) {
                //물표시
//                if(waterDetection.get(i) == "true")
//                {
//                    LimitLine xLimitLine = new LimitLine(i, "ㆁ");
//                    xLimitLine.setLineWidth(0f);
//                    xLimitLine.setTextSize(27f);
//                    int colorLine = getResources().getColor(R.color.bg3);
//                    xLimitLine.setLineColor(colorLine);
//                    xLimitLine.setTextColor(Color.BLUE);
//                    xAxis.addLimitLine(xLimitLine);
//                    xLimitLine.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
//                }
                if (i % 360 == 0 && i > 0) {
                    xCount++;
                    xaxes[i] = String.valueOf(xCount) + "시"; //+ (i*5) % 60 + "분";
                }
                else
                    xaxes[i] = "";
            }

            for (int i = 0; i < X/10; i++) {
                if(i * 10 >= a && count >= 0) // if ((i * 10) >= a && ((i-1) * 10) <= a && b == 0) <- 이건 데이터값이 끊기지 않을경우 가능
                {
                    yAXES2.add(new Entry(Float.parseFloat(soil.get(count)), i));
                    //Log.d("토양수분값",String.valueOf(soil.get(count)));
                    if (waterDetection.get(count) == "true") {
                        LimitLine xLimitLine = new LimitLine(i, "ㆁ");
                        xLimitLine.setLineWidth(0f);
                        xLimitLine.setTextSize(27f);
                        int colorLine = getResources().getColor(R.color.bg3);
                        xLimitLine.setLineColor(colorLine);
                        xLimitLine.setTextColor(Color.BLUE);
                        xAxis.addLimitLine(xLimitLine);
                        xLimitLine.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
                    }
                    count--;
//                    if (count >= time.size()) {
//                        b = 1;
//                        continue;
//                    }

                    strArr = time.get(count).split("T");
                    value = strArr[1].split("\\:");
                    secValue = value[2].split("\\.");

                    //Log.d("값 :", strArr[1]);
                    a = Integer.parseInt(value[0]) * 3600 + Integer.parseInt(value[1])* 60 + Integer.parseInt(secValue[0]) + (540*60);
                    if(a >= 86400)
                        a -= 86400;
                }
            }
        }
        lineDataSet2.setDrawCircles(false);
        int color = getResources().getColor(R.color.background);
        lineDataSet2.setColor(color);
        lineDataSet2.setLineWidth(3f);
        lineDataSet2.setDrawCubic(false);
        lineDataSet2.setValueTextSize(0.0f);
        lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSets.add(lineDataSet2);
        YAxis leftAxis = lineChart.getAxisLeft();
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setEnabled(false);
        leftAxis.setLabelCount(11, false); // y축 보여지는 숫자 갯수
        leftAxis.setStartAtZero(false);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setAxisMaxValue(2000f);
        leftAxis.setValueFormatter(new SoilFragment.MyYAxisValueFormatter());

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        if(xCount <= 18) {
            xAxis.setLabelsToSkip(11);
        }
        else
        {
            xAxis.setLabelsToSkip(15);
        }

        xAxis.setValueFormatter(new SoilFragment.MyCustomXAxisValueFormatter());

        lineChart.setDrawGridBackground(false);
        lineChart.setDescription("");

        lineChart.setMarkerView(mv);
        if(graph == 0) {
            lineChart.setData(new LineData(xaxes, lineDataSets));
        }
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_soil, container, false);

    }
}
