package com.example.kim.Eyes_on.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kim.Eyes_on.R;

/**
 * Created by Sangwoo Kim on 2017. 5. 29..
 */

public class NoConnectionFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_no_connection,container,false);
    }
}
