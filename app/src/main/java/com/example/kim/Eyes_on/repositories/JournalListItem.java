package com.example.kim.Eyes_on.repositories;

/**
 * Created by Sangwoo Kim on 2017. 11. 1..
 */

public class JournalListItem {
    private String year;
    private String monthofyear;
    private String dayofmonth;
    private String subjectStr;
    private int waterCount;
    private int refreshCount;
    private int journal_id;

    public String getYear() {
        return year;
    }

    public String getDayofmonth() {
        return dayofmonth;
    }

    public String getMonthofyear() {
        return monthofyear;
    }

    public int getRefreshCount() {
        return refreshCount;
    }

    public int getWaterCount() {
        return waterCount;
    }

    public String getSubjectStr() {
        return subjectStr;
    }

    public int getJournal_id() {
        return journal_id;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonthofyear(String monthofyear) {
        this.monthofyear = monthofyear;
    }

    public void setDayofmonth(String dayofmonth) {
        this.dayofmonth = dayofmonth;
    }

    public void setRefreshCount(int refreshCount) {
        this.refreshCount = refreshCount;
    }

    public void setSubjectStr(String subjectStr) {
        this.subjectStr = subjectStr;
    }

    public void setWaterCount(int waterCount) {
        this.waterCount = waterCount;
    }

    public void setJournal_id(int journal_id) {
        this.journal_id = journal_id;
    }

}
