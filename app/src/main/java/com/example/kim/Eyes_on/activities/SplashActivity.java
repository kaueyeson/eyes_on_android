package com.example.kim.Eyes_on.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.kim.Eyes_on.R;

/**
 * Created by Sangwoo Kim on 2017. 5. 12..
 */

public class SplashActivity extends AppCompatActivity {

    /**
     * postDelayed를 사용하여 1초간 EYES ON의 아이콘을 보여주고
     * 로그인을 위한 LoginActivity로 화면이 전환된다
     */


    /** 로딩 화면이 떠있는 시간(밀리초단위)  **/
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_splash);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this,LoginActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
