package com.example.kim.Eyes_on.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.kim.Eyes_on.repositories.ListViewItem;
import com.example.kim.Eyes_on.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Sangwoo Kim on 2017. 4. 30..
 */

public class ListViewAdapter extends BaseAdapter {
    @Bind(R.id.title_list) TextView titleTextView;
    @Bind(R.id.temp_list) TextView tempTextView;
    @Bind(R.id.humid_list) TextView humidTextView;

    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<ListViewItem> listViewItemList = new ArrayList<ListViewItem>();
    public ListViewAdapter(){

    }

    //Adapter에 사용되는 데이터의 갯수 리턴
    @Override
    public int getCount(){
        return listViewItemList.size();
    }

    //position에 위치한 데이터를 화면에 출력하는데 사용될 view 리턴
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final int pos = position;
        final Context context = parent.getContext();

        // Inflate layout
        if(convertView == null){
            LayoutInflater inflater
                    = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_sensor, parent, false);
        }

        ButterKnife.bind(this,convertView);

        ListViewItem listViewItem = listViewItemList.get(position);

        titleTextView.setText(listViewItem.getTitleStr());
        tempTextView.setText(listViewItem.getTempStr());
        humidTextView.setText(listViewItem.getHumidStr());


        return convertView;
    }

    // 지정한 position에 있는 데이터와 관계된 아이템의 ID 리턴
    @Override
    public long getItemId(int position){
        return position;
    }

    //  지정한 position에 있는 데이터 리턴
    public Object getItem(int position){
        return listViewItemList.get(position);
    }

    // 아이템 데이터 추가를 위한 함수.
    public void addItem(String title, String temp, String humid) {
        ListViewItem item = new ListViewItem();

        item.setTitleStr(title);
        item.setHumidStr(humid);
        item.setTempStr(temp);

        listViewItemList.add(item);
    }
}
