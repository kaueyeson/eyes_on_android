package com.example.kim.Eyes_on.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.adapters.ViewPagerAdapter;
import com.example.kim.Eyes_on.fragments.ReportFragment;
import com.example.kim.Eyes_on.fragments.WeatherFragment;

/**
 * Created by Sangwoo Kim
 */

public class MainActivity extends AppCompatActivity {

    /**
     * ViewPager를 이용하여 두가지 기능을 제공한다.
     * 기능 1. 실시간 날씨 정보와 생장환경 실시간 모니터링(온도, 습도)
     * 기능 2. 자동으로 생성되어 일부가 작성 된 일지 제공
     */

    private long pressedTime;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.mipmap.ic_weather,
            R.mipmap.ic_journal
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    @Override
    public void onBackPressed() {
        if(pressedTime ==0){
            Toast.makeText(MainActivity.this, "한번 더 누르면 종료됩니다", Toast.LENGTH_LONG).show();
            pressedTime = System.currentTimeMillis();
        } else {
            int seconds = (int) (System.currentTimeMillis() - pressedTime);

            if(seconds>2000) {
                pressedTime = 0;
            }
            else {
                moveTaskToBack(true);
                finish();
            }
        }
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new WeatherFragment(), "EYES_ON");
        adapter.addFragment(new ReportFragment(), "REPORT");
        viewPager.setAdapter(adapter);
    }
}
