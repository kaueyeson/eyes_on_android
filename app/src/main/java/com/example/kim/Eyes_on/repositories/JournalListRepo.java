package com.example.kim.Eyes_on.repositories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.ArrayList;


/**
 * Created by Sangwoo Kim on 2017. 11. 15..
 */

public class JournalListRepo {
    @SerializedName("data")
    @Expose
    public ArrayList<data> data = new ArrayList<>();
    public ArrayList<data> getData() {
        return data;
    }

    public class data {
        @SerializedName("journal_id") int journal_id;
        public int getJournal_id () {
            return journal_id;
        }

        @SerializedName("subject") String subject;
        public String getSubject() {
            return subject;
        }

        @SerializedName("content") String content;
        public String getContent() {
            return content;
        }

        @SerializedName("weather_status") String weather_status;
        public String getWeather_status () {
            return weather_status;
        }

        @SerializedName("datetime") String datetime;
        public String getDatetime() {
            return datetime;
        }


        @SerializedName("detection") detection detection;
        public class detection{
            @SerializedName("water_count") int water_count;
            public int getWater_count () {
                return water_count;
            }

            @SerializedName("refresh_count") int refresh_count;
            public int getRefresh_count () {
                return refresh_count;
            }
        }


        public data.detection getDetection() {
            return detection;
        }


    }
}
