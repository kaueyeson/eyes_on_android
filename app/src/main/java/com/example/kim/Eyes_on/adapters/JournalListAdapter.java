package com.example.kim.Eyes_on.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.repositories.JournalListItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Sangwoo Kim on 2017. 11. 1..
 */

public class JournalListAdapter extends BaseAdapter{
    @Bind(R.id.countspaceonlist) RelativeLayout countLayout;
    @Bind(R.id.subjectofreport) TextView subjectView;
    @Bind(R.id.watercount) TextView waterView;
    @Bind(R.id.refreshcount) TextView airView;
    @Bind(R.id.year_list) TextView tvYear_list;
    @Bind(R.id.monthofyear_list) TextView tvMonthofyear_list;
    @Bind(R.id.dayofmonth_list) TextView tvDayofmonth_list;

    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<JournalListItem> journalListItems = new ArrayList<JournalListItem>();
    public JournalListAdapter(){

    }

    //Adapter에 사용되는 데이터의 갯수 리턴
    @Override
    public int getCount(){
        return journalListItems.size();
    }

    //position에 위치한 데이터를 화면에 출력하는데 사용될 view 리턴
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final int pos = position;
        final Context context = parent.getContext();

        // Inflate layout
        if(convertView == null){
            LayoutInflater inflater
                    = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_journal, parent, false);
        }

        ButterKnife.bind(this,convertView);

        JournalListItem journalListItem = journalListItems.get(position);

        tvYear_list.setText(String.valueOf(journalListItem.getYear()));
        tvMonthofyear_list.setText(String.valueOf(journalListItem.getMonthofyear()));
        tvDayofmonth_list.setText(String.valueOf(journalListItem.getDayofmonth()));
        subjectView.setText(String.valueOf(journalListItem.getSubjectStr()));
        waterView.setText(String.valueOf(journalListItem.getWaterCount()));
        airView.setText(String.valueOf(journalListItem.getRefreshCount()));

        int watercount = journalListItem.getWaterCount();
        if(watercount==0) countLayout.setBackgroundColor(Color.RED);
        else if(watercount==1) countLayout.setBackgroundColor(Color.YELLOW);
        else countLayout.setBackgroundColor(Color.GREEN);

        return convertView;
    }

    // 지정한 position에 있는 데이터와 관계된 아이템의 ID 리턴
    @Override
    public long getItemId(int position){
        return position;
    }

    //  지정한 position에 있는 데이터 리턴
    public Object getItem(int position){
        return journalListItems.get(position);
    }

    public int getJournalId(int position){
        return journalListItems.get(position).getJournal_id();
    }

    // 아이템 데이터 추가를 위한 함수.
    public void addItem(String year, String month, String day, String subject, int water, int air, int journal_id) {
        JournalListItem item = new JournalListItem();

        item.setYear(year);
        item.setMonthofyear(month);
        item.setDayofmonth(day);
        item.setSubjectStr(subject);
        item.setWaterCount(water);
        item.setRefreshCount(air);
        item.setJournal_id(journal_id);

        journalListItems.add(item);
    }
}
