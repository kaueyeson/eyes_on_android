package com.example.kim.Eyes_on.fragments;

import android.animation.ArgbEvaluator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.Log;
import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.activities.GraphActivity;
import com.example.kim.Eyes_on.activities.MainActivity;
import com.example.kim.Eyes_on.MyMarkerView;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.ComponentBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.interfaces.LineDataProvider;
import com.github.mikephil.charting.renderer.LineChartRenderer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;


import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.Bind;

import static com.example.kim.Eyes_on.R.id.chart;

public class TempFragment extends Fragment{
    LineChart lineChart;
    public TempFragment() {
        // Required empty public constructor
    }

    public class MyYAxisValueFormatter implements YAxisValueFormatter {

        private DecimalFormat mFormat;

        public MyYAxisValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }


        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return mFormat.format(value) + "℃";
        }
    };
    public class MyCustomXAxisValueFormatter implements XAxisValueFormatter {

        @Override
        public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {

            if(viewPortHandler.getScaleX() > 1.0f) {
                viewPortHandler.hasNoDragOffset();
            }
            return original;
        }
    };
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GraphActivity repo = new GraphActivity();
        Context context = getActivity();
        MyMarkerView mv = new MyMarkerView(context,R.layout.content_marker_view);

        //ArrayList<String> temp = repo.getTemp();
        ArrayList<String> time = repo.getTimeList();
        ArrayList<String> waterDetection = repo.getWaterDetection(); //임시로 만듦
        for(int i=0;i<repo.getTemp().size();i++) {
            //Log.d("시간값", repo.getTimeList().get(i));

            //Log.d("워터",waterDetection.get(i));
        }

        lineChart = (LineChart) getActivity().findViewById(chart);
        XAxis xAxis = lineChart.getXAxis();
        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        ArrayList<Entry> values = new ArrayList<Entry>();
        LineDataSet set1 = new LineDataSet(values, "온도");
        int count = time.size() - 1;//int count = 0;
        int graph = 0;
        int b = 0;
        int xCount = 0;
        int size = 1;
        String[] XstrArr, strArr, Xvalue, value, secXvalue, secValue;
        int X = 0, a = 0;
        Log.d("timesize : ",String.valueOf(time.size()));
        if(time.size() != 0)
        {
            size = time.size();
            XstrArr = time.get(0).split("T");//XstrArr = time.get(size - 1).split("T"); //time.get(0)이 정상
            strArr = time.get(count).split("T");
            Xvalue = XstrArr[1].split("\\:");
            value = strArr[1].split("\\:");
            secValue = value[2].split("\\.");
            secXvalue = Xvalue[2].split("\\.");
            X = Integer.parseInt(Xvalue[0]) * 3600 + Integer.parseInt(Xvalue[1])* 60 + Integer.parseInt(secXvalue[0]) + (540*60); //초단위
            a = Integer.parseInt(value[0]) * 3600 + Integer.parseInt(value[1])*60 + Integer.parseInt(secValue[0]) + (540*60); //초단위
            if(X >= 86400)
            {
                X -= 86400;
            }
            if(a >= 86400)
            {
                a -= 86400;
            }
        }
        String[] xaxes = new String[X/10]; //10초단위

        Log.d("time.size",String.valueOf(time.size()));
        if(time.size() == 0)
        {
            Log.d("데이터값 없음",String.valueOf(time.size()));
            graph = 1;
        }
        else {

            Log.d("X =",String.valueOf(X));
            Log.d("a = ", String.valueOf(a));
            xaxes[0] = "0시";
            for (int i = 1; i < X / 10; i++) {//for(int i = X / 10; i > 0; i--){

                if (i % 360 == 0 && i > 0) {
                    xCount++;
                    xaxes[i] = String.valueOf(xCount) + "시"; //+ (i*5) % 60 + "분";
                    Log.d("시 : ",String.valueOf(xCount));
                }
                else
                    xaxes[i] = "";
                //Log.d("시 : ",String.valueOf(xCount));
            }

            for (int i = 0; i < X/10; i++) {
                if(i * 10 >= a && count >= 0) // if ((i * 10) >= a && ((i-1) * 10) <= a && b == 0) <- 이건 데이터값이 끊기지 않을경우 가능
                {
                    values.add(new Entry(Float.parseFloat(repo.getTemp().get(count)), i));
                    //Log.d("값",String.valueOf(repo.getTemp().get(count)));
                    if (waterDetection.get(count) == "true") {
                        LimitLine xLimitLine = new LimitLine(i, "ㆁ");
                        xLimitLine.setLineWidth(0f);
                        xLimitLine.setTextSize(27f);
                        int colorLine = getResources().getColor(R.color.bg3);
                        xLimitLine.setLineColor(colorLine);
                        xLimitLine.setTextColor(Color.BLUE);
                        xAxis.addLimitLine(xLimitLine);
                        xLimitLine.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
                    }
                    count--;

//                    if (count >= time.size()) {
//                        b = 1;
//                        continue;
//                    }
                    strArr = time.get(count).split("T");
                    value = strArr[1].split("\\:");
                    secValue = value[2].split("\\.");
                    //Log.d("값 :", strArr[1]);
                    a = Integer.parseInt(value[0]) * 3600 + Integer.parseInt(value[1])* 60 + Integer.parseInt(secValue[0]) + (540*60);
                    if(a >= 86400)
                        a -= 86400;
                }
            }
        }
        set1.setDrawCircles(false);
        int color = getResources().getColor(R.color.background);
        set1.setColor(color);
        set1.setDrawCubic(false);
        set1.setValueTextSize(0.0f);
        set1.setLineWidth(3f);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSets.add(set1);
        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setLabelCount(11, false); // y축 보여지는 숫자 갯수
        leftAxis.setStartAtZero(false);
        leftAxis.setAxisMinValue(0f);
        leftAxis.setAxisMaxValue(50f);
        leftAxis.setValueFormatter(new TempFragment.MyYAxisValueFormatter());
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setEnabled(false);


        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        if(xCount <= 18) {
            xAxis.setLabelsToSkip(11);
        }
        else
        {
            xAxis.setLabelsToSkip(15);
        }


        xAxis.setValueFormatter(new TempFragment.MyCustomXAxisValueFormatter());

        lineChart.setDrawGridBackground(false);
        lineChart.setDescription("");
        lineChart.setMarkerView(mv);
        if(graph == 0) {
            lineChart.setData(new LineData(xaxes, lineDataSets));
        }
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_temp, container, false);
    }

}
