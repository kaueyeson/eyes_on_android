package com.example.kim.Eyes_on.retrofit;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by Sangwoo Kim on 2017. 11. 26..
 */

public class RetrofitClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
