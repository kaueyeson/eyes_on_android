package com.example.kim.Eyes_on.repositories;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sangwoo Kim on 2017. 6. 12..
 */

public class UsersRepo {

    @SerializedName("phone_number") private String phone_number;

    public String getPhone_number() {
        return phone_number;
    }

    @SerializedName("password") private String password;

    public String getPassword() {
        return password;
    }

    @SerializedName("email") private String email;

    public String getEmail() {
        return email;
    }

    @SerializedName("first_name") private String first_name;

    public String getFirst_name() {
        return first_name;
    }

    @SerializedName("last_name") private String last_name;

    public String getLast_name() {
        return last_name;
    }

    @SerializedName("dob_year") private int dob_year;

    public int getDob_year() {
        return dob_year;
    }

    @SerializedName("dob_month") private int dob_month;

    public int getDob_month() {
        return dob_month;
    }

    @SerializedName("dob_day") private int dob_day;

    public int getDob_day() {
        return dob_day;
    }

    @SerializedName("gender") private String gender;

    public String getGender() {
        return gender;
    }

    @SerializedName("job") private String job;

    public String getJob() {
        return job;
    }
}

