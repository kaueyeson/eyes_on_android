package com.example.kim.Eyes_on.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.apiInterfaces.WeatherApiInterface;
import com.example.kim.Eyes_on.repositories.WeatherRepo;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Sangwoo Kim on 2017. 5. 29..
 */

public class WeatherFragment extends Fragment {
    private static final String TAG = "WeatherFragment";

    @Bind(R.id.weather_img) ImageView wi;
    @Bind(R.id.v_humid) TextView v_humid;
    @Bind(R.id.v_temp) TextView v_temp;
    @Bind(R.id.v_windspd) TextView v_windspd;
    @Bind(R.id.updatetime) TextView updatetime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        ButterKnife.bind(this,view);

        getWeather();

        return view;
    }

    public void getWeather(){
        final String lat = "38.2772";
        final String lon = "127.2058";
        final int version = 1;

        Retrofit client = new Retrofit.Builder().baseUrl("http://apis.skplanetx.com/").addConverterFactory(GsonConverterFactory.create()).build();
        WeatherApiInterface service = client.create(WeatherApiInterface.class);
        Call<WeatherRepo> call = service.get_Weather_retrofit(version, lat, lon);
        call.enqueue(new Callback<WeatherRepo>() {
            @Override
            public void onResponse(Call<WeatherRepo> call, Response<WeatherRepo> response) {
                if(response.isSuccessful()){
                    WeatherRepo repo = response.body();

                    Log.d(TAG,String.valueOf(repo.getWeather().getMinutely().size()));
                    if(repo.getResult().getCode().equals("9200")){ // 9200 = 성공
                        strToicon(repo.getWeather().getMinutely().get(0).getSky().getCode());
                        v_temp.setText(repo.getWeather().getMinutely().get(0).getTemperature().getTc());
                        v_humid.setText(repo.getWeather().getMinutely().get(0).getHumidity());
                        v_windspd.setText(repo.getWeather().getMinutely().get(0).getWind().getWspd());
                        updatetime.setText(repo.getWeather().getMinutely().get(0).getTimeObservation());
                    }else{}
                }
            }
            @Override
            public void onFailure(Call<WeatherRepo> call, Throwable t) {}
        });
    }

    public void strToicon(String code) {
        Calendar c = Calendar.getInstance();

        if (code.equals("SKY_A01")) {
            if(c.get(Calendar.HOUR_OF_DAY)>=18 || c.get(Calendar.HOUR_OF_DAY)<6)
                wi.setImageResource(R.drawable.w08);
            else
                wi.setImageResource(R.drawable.w01);
        } else if (code.equals("SKY_A02")) {
            if(c.get(Calendar.HOUR_OF_DAY)>=18 || c.get(Calendar.HOUR_OF_DAY)<6)
                wi.setImageResource(R.drawable.w09);
            else
                wi.setImageResource(R.drawable.w02);

        } else if (code.equals("SKY_A03")) {
            if(c.get(Calendar.HOUR_OF_DAY)>=18 || c.get(Calendar.HOUR_OF_DAY)<6)
                wi.setImageResource(R.drawable.w10);
            else
                wi.setImageResource(R.drawable.w03);
        } else if (code.equals("SKY_A04")) {
            if(c.get(Calendar.HOUR_OF_DAY)>=18 || c.get(Calendar.HOUR_OF_DAY)<6)
                wi.setImageResource(R.drawable.w40);
            else
                wi.setImageResource(R.drawable.w12);
        } else if (code.equals("SKY_A05")) {
            if(c.get(Calendar.HOUR_OF_DAY)>=18 || c.get(Calendar.HOUR_OF_DAY)<6)
                wi.setImageResource(R.drawable.w41);
            else
                wi.setImageResource(R.drawable.w13);
        } else if (code.equals("SKY_A06")) {
            if(c.get(Calendar.HOUR_OF_DAY)>=18 || c.get(Calendar.HOUR_OF_DAY)<6)
                wi.setImageResource(R.drawable.w42);
            else
                wi.setImageResource(R.drawable.w14);
        } else if (code.equals("SKY_A07")) {
            wi.setImageResource(R.drawable.w18);
        } else if (code.equals("SKY_A08")) {
            wi.setImageResource(R.drawable.w21);
        } else if (code.equals("SKY_A09")) {
            wi.setImageResource(R.drawable.w32);
        } else if (code.equals("SKY_A10")) {
            wi.setImageResource(R.drawable.w04);
        } else if (code.equals("SKY_A11")) {
            wi.setImageResource(R.drawable.w29);
        } else if (code.equals("SKY_A12")) {
            wi.setImageResource(R.drawable.w26);
        } else if (code.equals("SKY_A13")) {
            wi.setImageResource(R.drawable.w27);
        } else if (code.equals("SKY_A14")) {
            wi.setImageResource(R.drawable.w28);
        }
    }
}
