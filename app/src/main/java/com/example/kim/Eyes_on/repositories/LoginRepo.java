package com.example.kim.Eyes_on.repositories;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sangwoo Kim on 2017. 11. 30..
 */

public class LoginRepo {
    @SerializedName("user_id") private int user_id;
    @SerializedName("access_token") private String token;

    public int getUser_id() {
        return user_id;
    }

    public String getToken() {
        return token;
    }

}
