package com.example.kim.Eyes_on.apiInterfaces;

import com.example.kim.Eyes_on.repositories.WeatherRepo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Sangwoo Kim on 2017. 4. 6..
 */

public interface WeatherApiInterface {
    @Headers({"Accept: application/json","appKey: 18e5bb3a-f6d6-3e57-8ff6-24aba276572d"})
    @GET("weather/current/minutely")
    Call<WeatherRepo> get_Weather_retrofit(@Query("version") int version, @Query("lat") String lat, @Query("lon") String lon);
}
