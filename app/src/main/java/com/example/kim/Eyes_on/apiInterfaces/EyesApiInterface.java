package com.example.kim.Eyes_on.apiInterfaces;

import com.example.kim.Eyes_on.repositories.EyesOnRepo;
import com.example.kim.Eyes_on.repositories.JournalListRepo;
import com.example.kim.Eyes_on.repositories.JournalRepo;

import org.joda.time.LocalDateTime;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Sangwoo Kim on 2017. 5. 7..
 */

public interface EyesApiInterface {
    @Headers({"Accept: application/json"})
    @GET("v1/users/self/devices/{device_type}/{device_id}/events/raw-data")
    Call<EyesOnRepo> get_Eyeson_retrofit(@Header("Authorization") String authorization, @Path("device_type") String device_type,
                                         @Path("device_id") int device_id);

    @GET("v1/users/self/devices/{device_type}/{device_id}/events/raw-data")
    Call<EyesOnRepo> get_Eyeson_retrofit2(@Header("Authorization") String authorization, @Path("device_type") String device_type,
                                         @Path("device_id") int device_id, @Query("from") LocalDateTime from, @Query("to") LocalDateTime to);

    @GET("v1/users/self/journals")
    Call<JournalListRepo> get_JournalList_retrofit(@Header("Authorization") String authorization);

    @GET("v1/users/self/journals/{journal_id}")
    Call<JournalRepo> get_Journal_retrofit(@Header("Authorization") String authorization, @Path("journal_id") int journal_id);


    @PATCH("v1/users/self/journals/{journal_id}")
    Call<JournalRepo> set_Journal_retrofit(@Header("Authorization") String authorization, @Path("journal_id") int journal_id,
                                           @Body RequestBody body);
}
