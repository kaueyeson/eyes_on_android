package com.example.kim.Eyes_on.repositories;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

/**
 * Created by Sangwoo Kim on 2017. 11. 22..
 */

public class JournalRepo {

    @SerializedName("journal_id") private int journal_id;
    public int getJournal_id() {
        return journal_id;
    }

    @SerializedName("user_id") private int user_id;
    public int getUser_id() {
        return user_id;
    }

    @SerializedName("subject") private String subject;

    public String getSubject() {
        return subject;
    }

    @SerializedName("content") private  String content;

    public String getContent() {
        return content;
    }

    @SerializedName("datetime") private  String datetime;

    public String getDatetime() {
        return datetime;
    }

    @SerializedName("weather") private weather weather;
    public class weather{
        @SerializedName("status") String status;
        public String getStatus() {
            return status;
        }

        @SerializedName("daily_max_temp") double max_temp;
        public double getMax_temp() {
            return max_temp;
        }

        @SerializedName("daily_min_temp") double min_temp;
        public double getMin_temp() {
            return min_temp;
        }

        @SerializedName("daily_humid") double daily_humid;
        public double getDaily_humid() {
            return daily_humid;
        }
    }

    @SerializedName("detection") private detection detection;
    public class detection{
        @SerializedName("water_count") int water_count;

        public int getWater_count() {
            return water_count;
        }

        @SerializedName("refresh_count") int refresh_count;

        public int getRefresh_count() {
            return refresh_count;
        }
    }

    public detection getDetection() {
        return detection;
    }

    public JournalRepo.weather getWeather() {
        return weather;
    }
}
