package com.example.kim.Eyes_on.repositories;

/**
 * Created by Sangwoo Kim on 2017. 4. 30..
 */

public class ListViewItem {
    private String titleStr;
    private String humidStr;
    private String tempStr;

    public void setTitleStr(String title){
        titleStr = title;
    }
    public void setTempStr(String tempStr) {
        this.tempStr = tempStr;
    }
    public void setHumidStr(String humid){
        humidStr = humid;
    }

    public String getTitleStr(){
        return this.titleStr;
    }
    public String getTempStr() {
        return tempStr;
    }
    public String getHumidStr(){
        return this.humidStr;
    }

}
