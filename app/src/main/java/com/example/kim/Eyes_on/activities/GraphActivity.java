package com.example.kim.Eyes_on.activities;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.adapters.ListViewAdapter;
import com.example.kim.Eyes_on.apiInterfaces.EyesApiInterface;
import com.example.kim.Eyes_on.fragments.HumiFragment;
import com.example.kim.Eyes_on.fragments.SoilFragment;
import com.example.kim.Eyes_on.fragments.TempFragment;
import com.example.kim.Eyes_on.repositories.EyesOnRepo;
import com.example.kim.Eyes_on.retrofit.ApiUtils;

import org.joda.time.LocalDateTime;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.kim.Eyes_on.R.id.btn_humi;
import static com.example.kim.Eyes_on.R.id.btn_next;
import static com.example.kim.Eyes_on.R.id.btn_prev;
import static com.example.kim.Eyes_on.R.id.btn_signup;
import static com.example.kim.Eyes_on.R.id.btn_soil;
import static com.example.kim.Eyes_on.R.id.btn_temp;
import static com.example.kim.Eyes_on.R.id.time;


/**
 * Created by 김수봉 on 2017-05-16.
 */

public class GraphActivity extends AppCompatActivity implements View.OnClickListener{
    private final int FRAGMENT1 = 1;
    private final int FRAGMENT2 = 2;
    private final int FRAGMENT3 = 3;
    private Button btnTemp, btnHumi, btnSoil, btnNext, btnPrev;
    int key = 0;
    int checkGraph = 1; //온도그래프
    int showGraph = 1;
    int msg;
    Intent intent;
    ListViewAdapter adapter;
    private EyesApiInterface api;
    private String token;
    private int id;
    private static final String TAG = "GraphActivity";
    static final ArrayList<String> Temp = new ArrayList<>();
    static final ArrayList<String> Humi = new ArrayList<>();
    static final ArrayList<String> TimeList = new ArrayList<>();
    static final ArrayList<String> WaterDetection = new ArrayList<>(); //임시로 만듬
    static final ArrayList<String> Soil = new ArrayList<>();
    LocalDateTime Time = LocalDateTime.now();//.minusHours(9);
    LocalDateTime fromTime = Time.withHourOfDay(15).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).minusDays(1);

    LocalDateTime checkTime = Time;
    TextView textview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("뒤로 가기");
        textview = (TextView) findViewById(R.id.textView);
        textview.setText(String.valueOf(Time.getYear())+"-"+String.valueOf(Time.getMonthOfYear())+"-"+String.valueOf(Time.getDayOfMonth()));
        Time = Time.minusHours(9);
        intent = getIntent();
        msg = intent.getIntExtra("msg",-1);
        api = ApiUtils.getEyesAPIService();
        token = LoginActivity.getToken();
        id = LoginActivity.getId();
       switch (msg)
        {
            case 1:
                Temp.clear();
                Humi.clear();
                TimeList.clear();
                WaterDetection.clear();
                Soil.clear();
                Time = LocalDateTime.now();//.minusHours(9);
                fromTime = Time.withHourOfDay(15).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).minusDays(1);
                Time = Time.minusHours(9);
                Log.d("1번 리스트뷰","ok");
                getEyeson();
                break;
            case 0:
                Log.d("2번 리스트뷰","ok");
                Temp.clear();
                Humi.clear();
                TimeList.clear();
                WaterDetection.clear();
                Soil.clear();
                Time = LocalDateTime.now();//.minusHours(9);
                fromTime = Time.withHourOfDay(15).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).minusDays(1);
                Time = Time.minusHours(9);
                getEyeson2();
                break;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnTemp = (Button) findViewById(btn_temp);
        btnHumi = (Button) findViewById(btn_humi);
        btnSoil = (Button) findViewById(btn_soil);
        btnPrev = (Button) findViewById(btn_prev);
        btnNext = (Button) findViewById(btn_next);
        btnTemp.setOnClickListener(this);
        btnHumi.setOnClickListener(this);
        btnSoil.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        int color = getResources().getColor(R.color.background);
        btnPrev.setBackgroundColor(color);
        btnNext.setBackgroundColor(color);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_temp :
                // '버튼1' 클릭 시 '프래그먼트1' 호출
                callFragment(FRAGMENT1);
                checkGraph = 1;
                break;
            case R.id.btn_humi :
                // '버튼2' 클릭 시 '프래그먼트2' 호출
                callFragment(FRAGMENT2);
                checkGraph = 2;
                break;
            case R.id.btn_soil :
                // '버튼2' 클릭 시 '프래그먼트2' 호출
                callFragment(FRAGMENT3);
                checkGraph = 3;
                break;
            case R.id.btn_prev:
                //Time = Time.minusHours(24);
                Time = Time.plusHours(9);
                Time = Time.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(0).minusDays(1);

                //fromTime = fromTime.minusHours(24);
                fromTime = Time.plusMinutes(1).minusDays(1);
                textview.setText(String.valueOf(Time.getYear())+"-"+String.valueOf(Time.getMonthOfYear())+"-"+String.valueOf(Time.getDayOfMonth()));
                Time = Time.minusHours(9);
                if(checkGraph == 1)
                {
                    showGraph = 1;
                    if(msg == 1) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson();
                    }
                    else if(msg == 0) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson2();
                    }
                    //callFragment(FRAGMENT1);
                }
                else if(checkGraph == 2)
                {
                    showGraph = 2;
                    if(msg == 1) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson();
                    }
                    else if(msg == 0) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson2();
                    }
                    //callFragment(FRAGMENT2);
                }
                else if(checkGraph == 3)
                {
                    showGraph = 3;
                    if(msg == 1) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson();
                    }
                    else if(msg == 0) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson2();
                    }
                    //callFragment(FRAGMENT3);
                }

                break;
            case R.id.btn_next:
                Time = Time.plusDays(1);
                fromTime = fromTime.plusHours(24);
                textview.setText(String.valueOf(Time.getYear())+"-"+String.valueOf(Time.getMonthOfYear())+"-"+String.valueOf(Time.getDayOfMonth()));

                if(Time.getDayOfMonth() == checkTime.getDayOfMonth()) {
                    Time = checkTime.minusHours(9);//Time = Time.plusHours(24);
                    fromTime = fromTime.minusHours(9);
                }


                if(checkGraph == 1)
                {
                    showGraph = 1;
                    if(msg == 1) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson();
                    }
                    else if(msg == 0) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson2();
                    }
                    //callFragment(FRAGMENT1);
                }
                else if(checkGraph == 2)
                {
                    showGraph = 2;
                    if(msg == 1) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson();
                    }
                    else if(msg == 0) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson2();
                    }
                    //callFragment(FRAGMENT2);
                }
                else if(checkGraph == 3)
                {
                    showGraph = 3;
                    if(msg == 1) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson();
                    }
                    else if(msg == 0) {
                        Temp.clear();
                        Humi.clear();
                        TimeList.clear();
                        WaterDetection.clear();
                        Soil.clear();
                        getEyeson2();
                    }
                    //callFragment(FRAGMENT3);
                }
                break;
        }
    }
    private void callFragment(int frament_no){
        // 프래그먼트 사용을 위해
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        switch (frament_no){
            case 1:
                // '프래그먼트1' 호출
                TempFragment tempfragment = new TempFragment();
                transaction.replace(R.id.fragment_container, tempfragment);
                transaction.commit();
                break;

            case 2:
                // '프래그먼트2' 호출
                HumiFragment humifragment = new HumiFragment();
                transaction.replace(R.id.fragment_container, humifragment);
                transaction.commit();
                break;
            case 3:
                SoilFragment soilfragment = new SoilFragment();
                transaction.replace(R.id.fragment_container, soilfragment);
                transaction.commit();
                break;
        }
    }

    public void getEyeson() {
        final String device_type = "seed";
        Log.d("userToken", token);
        Log.d("타임변화",String.valueOf(Time));
        Log.d("타임변화from",String.valueOf(fromTime));
        api.get_Eyeson_retrofit2(token, device_type, 1, fromTime, Time).enqueue(new Callback<EyesOnRepo>() {
            @Override
            public void onResponse(Call<EyesOnRepo> call, Response<EyesOnRepo> response) {
                String getCode = Integer.toString(response.code());
                if (response.isSuccessful()) {
                    Log.d(TAG, getCode);
                    if (response.code() == 200) {
                        try {
                            EyesOnRepo repo = response.body();

                            Log.d("데이터크기",String.valueOf(repo.data.size()));
                            //온습도그래프 작성을 위한 데이터 수집
                            for (int idx = 0; idx < repo.data.size(); idx++) {
                                Temp.add(repo.getData().get(idx).getSensor_value().getTemp());
                                Humi.add(repo.getData().get(idx).getSensor_value().getHumid());
                                TimeList.add(repo.getData().get(idx).getTimestamp().getDate());
                                WaterDetection.add(repo.getData().get(idx).getDetection().getWaterDetection());
                                Soil.add(repo.getData().get(idx).getSensor_value().getSoil());
                            }Log.d("데이터받지?","ok");
                            key++;
                                if(key == 1) {
                                    callFragment(FRAGMENT1);
                                }
                                else {
                                    if (showGraph == 1) {
                                        callFragment(FRAGMENT1);
                                    } else if (showGraph == 2) {
                                        callFragment(FRAGMENT2);
                                    } else if (showGraph == 3) {
                                        callFragment(FRAGMENT3);
                                    }
                                }

                            } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("여기","1");
//                            Temp.clear();
//                            Humi.clear();
//                            TimeList.clear();
//                            WaterDetection.clear();
//                            Soil.clear();
                            Log.d(TAG, getCode);
                        }
                    }
                } else {

                    Log.d("여기","2");
                    Log.d(TAG, getCode);
                }
            }
            @Override
            public void onFailure(Call<EyesOnRepo> call, Throwable t) {
                Log.d(TAG, t.toString());
            }
        });
    }
    public void getEyeson2() {
        final String device_type = "seed";

        Log.d("userToken", token);

        api.get_Eyeson_retrofit2(token, device_type, 2, fromTime, Time).enqueue(new Callback<EyesOnRepo>() {
            @Override
            public void onResponse(Call<EyesOnRepo> call, Response<EyesOnRepo> response) {
                String getCode = Integer.toString(response.code());
                if (response.isSuccessful()) {
                    Log.d(TAG, getCode);
                    if (response.code() == 200) {
                        try {
                            EyesOnRepo repo = response.body();

                            //온습도그래프 작성을 위한 데이터 수집
                            for (int idx = 0; idx < repo.data.size(); idx++) {
                                Temp.add(repo.getData().get(idx).getSensor_value().getTemp());
                                Humi.add(repo.getData().get(idx).getSensor_value().getHumid());
                                TimeList.add(repo.getData().get(idx).getTimestamp().getDate());
                                WaterDetection.add(repo.getData().get(idx).getDetection().getWaterDetection());
                                Soil.add(repo.getData().get(idx).getSensor_value().getSoil());
                            }
                            key++;
                            if(key == 1) {
                                callFragment(FRAGMENT1);
                            }
                            else {
                                if (showGraph == 1) {
                                    callFragment(FRAGMENT1);
                                } else if (showGraph == 2) {
                                    callFragment(FRAGMENT2);
                                } else if (showGraph == 3) {
                                    callFragment(FRAGMENT3);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, getCode);
                        }
                    }
                } else {
                    Log.d(TAG, getCode);
                }
            }

            @Override
            public void onFailure(Call<EyesOnRepo> call, Throwable t) {
                Log.d(TAG, t.toString());
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_tab, menu); //메뉴
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_settings:{
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent mainIntent = new Intent(GraphActivity.this,MainActivity.class);
        GraphActivity.this.startActivity(mainIntent);
        GraphActivity.this.finish();
    }

    public ArrayList<String> getTemp()
    {
        return Temp;
    }
    public ArrayList<String> getHumi()
    {
        return Humi;
    }
    public ArrayList<String> getSoil()
    {
        return Soil;
    }
    public ArrayList<String> getTimeList() {
        return TimeList;
    }
    public ArrayList<String> getWaterDetection() {return WaterDetection;}//임시로 만듦

}

