package com.example.kim.Eyes_on.fragments;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.example.kim.Eyes_on.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Sangwoo Kim on 2017. 10. 15..
 */

public class ReportFragment extends Fragment{
    @Bind(R.id.selectMonth) Spinner sm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this, view);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.journalList, new JournalListFragment());
        fragmentTransaction.commit();

        return view;
    }
}
