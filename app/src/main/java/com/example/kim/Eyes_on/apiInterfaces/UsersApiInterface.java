package com.example.kim.Eyes_on.apiInterfaces;

import com.example.kim.Eyes_on.repositories.LoginRepo;
import com.example.kim.Eyes_on.repositories.UsersRepo;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Sangwoo Kim on 2017. 11. 22..
 */

public interface UsersApiInterface {
    @Headers("Accept:application/json")

    @GET("v1/users/self")
    Call<UsersRepo> get_Owner_retrofit();

    @POST("v1/users")
    Call<UsersRepo> addUser(@Body RequestBody body);

    @POST("v1/users/login")
    Call<LoginRepo> login(@Body RequestBody body);

}
