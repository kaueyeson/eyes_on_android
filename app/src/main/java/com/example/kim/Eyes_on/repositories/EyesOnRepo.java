package com.example.kim.Eyes_on.repositories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by Sangwoo Kim on 2017. 5. 8..
 */

public class EyesOnRepo {

    @SerializedName("data")
    @Expose
    public ArrayList<data> data = new ArrayList<>();
    public ArrayList<data> getData() {return data;}

    public class data {

        @SerializedName("timestamp") timestamp timestamp;
        @SerializedName("sensor_value") sensor_value sensor_value;

        @SerializedName("detection") detection detection; //임시로 만듦


        public class timestamp{
            @SerializedName("$date") String date;
            public String getDate() {
                return date;
            }
        }

        public class detection{ //임시로 만듦
            @SerializedName("water_detection") Boolean water;
            @SerializedName("refresh_detection") Boolean refresh;

            public String getWaterDetection()
            {
                String s_water = Boolean.toString(water);
                return s_water;
            }
            public String getRefreshDetection()
            {
                String s_refresh = Boolean.toString(refresh);
                return s_refresh;
            }
        }

        public class sensor_value{
            @SerializedName("temp") Double temp;
            @SerializedName("humid") Double humid;
            @SerializedName("soil") Double soil;

            public String getTemp() {
                String s_temp = Double.toString(temp);
                return s_temp;
            }
            public String getHumid() {
                String s_humid = Double.toString(humid);
                return s_humid;
            }
            public String getSoil() {
                String s_soil = Double.toString(soil);
                return s_soil;
            }
        }

        public data.sensor_value getSensor_value() {return sensor_value;}
        public data.timestamp getTimestamp() {return timestamp;}
        public data.detection getDetection() {return detection;}
    }
}
