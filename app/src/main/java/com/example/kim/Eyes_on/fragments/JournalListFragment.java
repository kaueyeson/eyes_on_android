package com.example.kim.Eyes_on.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.kim.Eyes_on.retrofit.ApiUtils;
import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.activities.LoginActivity;
import com.example.kim.Eyes_on.adapters.JournalListAdapter;
import com.example.kim.Eyes_on.apiInterfaces.EyesApiInterface;
import com.example.kim.Eyes_on.repositories.JournalListRepo;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sangwoo Kim on 2017. 11. 1..
 */

public class JournalListFragment extends Fragment {
    @Bind(R.id.listview) ListView listview;
    private static int journal_id;
    public static int getJournal_id() {
        return journal_id;
    }

    private EyesApiInterface api;
    private String TAG = "journalList";

    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_journallist, container, false);
        ButterKnife.bind(this, view);
        api = ApiUtils.getEyesAPIService();
        token = LoginActivity.getToken();

        sendGet();

        return view;
    }

    public void sendGet(){
        api.get_JournalList_retrofit(token).enqueue(new Callback<JournalListRepo>() {
            @Override
            public void onResponse(Call<JournalListRepo> call, Response<JournalListRepo> response) {
                String getCode = Integer.toString(response.code());
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        try {
                            Log.d(TAG,getCode);
                            JournalListRepo repo = response.body();

                            final JournalListAdapter adapter;
                            adapter = new JournalListAdapter();
                            listview.setAdapter(adapter);

                            for(int idx=0;idx<repo.getData().size();idx++) {
                                String[] year_month = repo.getData().get(idx).getDatetime().split("-");
                                String[] day = year_month[2].split("T");

                                String _year = year_month[0];
                                String _month = year_month[1];
                                String _day = day[0];
                                String subject = repo.getData().get(idx).getSubject();

                                int waterCount = repo.getData().get(idx).getDetection().getWater_count();
                                int refreshCount = repo.getData().get(idx).getDetection().getRefresh_count();
                                int id = repo.getData().get(idx).getJournal_id();

                                adapter.addItem(_year, _month, _day, subject, waterCount, refreshCount ,id);
                            }

                            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                // 각 비닐하우스 아이템 클릭 시, 해당 비닐하우스의 온습도 그래프 페이지로 화면전환
                                @Override
                                public void onItemClick(AdapterView parent, View v, int position, long id) {
                                    journal_id = adapter.getJournalId(position);
                                    FragmentManager fm = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
                                    fragmentTransaction.add(R.id.journalList, new JournalFragment());
                                    fragmentTransaction.commit();
                                }
                            }) ;
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, getCode);
                        }
                    } else Log.d(TAG, getCode);
                }
            }

            @Override
            public void onFailure(Call<JournalListRepo> call, Throwable t) {
                Log.e(TAG, "Unable to get from API.");
            }
        });
    }
}