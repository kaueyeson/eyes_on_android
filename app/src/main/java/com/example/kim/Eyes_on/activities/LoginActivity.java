package com.example.kim.Eyes_on.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kim.Eyes_on.retrofit.ApiUtils;
import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.apiInterfaces.UsersApiInterface;
import com.example.kim.Eyes_on.repositories.LoginRepo;

import org.json.JSONObject;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Bind;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sangwoo Kim
 */

public class LoginActivity extends AppCompatActivity {

    /**
     * ID : phone_number
     * 로그인 성공 시 Authorization을 위한 token과 user_id를 서버로부터 부여받는다.
     * 모든 데이터들을 받으려면 user의 고유 token이 필요하다
     */

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private UsersApiInterface api;
    private static String token;
    private static int id;

    public static String getToken() {
        return "Bearer "+ token;
    }
    public static int getId() {
        return id;
    }

    @Bind(R.id.input_phone) EditText _phoneText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.btn_login) Button _loginButton;
    @Bind(R.id.btn_signup) Button _signupButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        api = ApiUtils.getUsersAPIService();

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(LoginActivity.this,SignupActivity.class);
                LoginActivity.this.startActivity(mainIntent);
                LoginActivity.this.finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("로그인 진행중...");
        progressDialog.show();

        final String phone = _phoneText.getText().toString();
        final String password = _passwordText.getText().toString();

        Map<String, Object> params = new ArrayMap<>();
        params.put("phone_number",phone);
        params.put("password",password);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8")
                ,(new JSONObject(params)).toString());

        api.login(body).enqueue(new Callback<LoginRepo>() {
            @Override
            public void onResponse(Call<LoginRepo> call, Response<LoginRepo> response) {
                String getCode = Integer.toString(response.code());
                Log.d(TAG+": Response", getCode);
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        LoginRepo repo = response.body();
                        token = repo.getToken();
                        id = repo.getUser_id();

                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        onLoginSuccess();
                                        progressDialog.dismiss();
                                    }
                                }, 1000);
                    }
                } else {
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    onLoginFailed();
                                    progressDialog.dismiss();
                                }
                            }, 1000);
                }
            }

            @Override
            public void onFailure(Call<LoginRepo> call, Throwable t) {
                Log.d(TAG, t.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Disable going back to any Activity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.fail_to_login), Toast.LENGTH_SHORT).show();
        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String phone = _phoneText.getText().toString();
        String password = _passwordText.getText().toString();

        if (phone.isEmpty() || phone.length()<10 || phone.length()>11){
            _phoneText.setError(getString(R.string.error_invalid_phone));
            valid = false;
        } else {
            _phoneText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4) {
            _passwordText.setError(getString(R.string.error_invalid_password));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}
