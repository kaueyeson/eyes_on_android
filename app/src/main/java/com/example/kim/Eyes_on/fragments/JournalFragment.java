package com.example.kim.Eyes_on.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kim.Eyes_on.retrofit.ApiUtils;
import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.activities.LoginActivity;
import com.example.kim.Eyes_on.apiInterfaces.EyesApiInterface;
import com.example.kim.Eyes_on.repositories.JournalRepo;

import org.json.JSONObject;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sangwoo Kim on 2017. 10. 15..
 */

public class JournalFragment extends Fragment {

    @Bind(R.id.cancelButton) TextView tvCancel;
    @Bind(R.id.setButton) TextView btnSet;
    @Bind(R.id.editButton) TextView btnEdit;
    @Bind(R.id.editTitle) EditText editTitle;
    @Bind(R.id.reportTitle) TextView tvTitle;

    @Bind(R.id.reportContent) TextView tvContent;
    @Bind(R.id.editContent) EditText editContent;
    @Bind(R.id.prevButton) TextView btnPrev;

    @Bind(R.id.prevDay) ImageView ivPrevDay;
    @Bind(R.id.nextDay) ImageView ivNextDay;
    @Bind(R.id.year) TextView tvYear;
    @Bind(R.id.monthofyear) TextView tvMonthofyear;
    @Bind(R.id.dayofmonth) TextView tvDayofmonth;
    @Bind(R.id.maxt) TextView tvMaxt;
    @Bind(R.id.mint) TextView tvMint;
    @Bind(R.id.dailyHumid) TextView tvHumid;
    @Bind(R.id.watercount) TextView tvWatercount;
    @Bind(R.id.refreshcount) TextView tvRefreshcount;
    @Bind(R.id.weather_img22) ImageView ivWeatherstatus;
    @Bind(R.id.countspace) RelativeLayout countSpace;

    private EyesApiInterface api;
    String TAG = "JournalFragment";
    private String token;
    private int id;
    private int journal_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_journal, container, false);

        ButterKnife.bind(this, view);
        api = ApiUtils.getEyesAPIService();
        token = LoginActivity.getToken();
        id = LoginActivity.getId();
        journal_id = JournalListFragment.getJournal_id();

        sendGet();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                sendGet();

            }
        }, 3000, 3000);

        ivPrevDay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                sendGet();

            }
        });

        ivNextDay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                sendGet();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvTitle.setVisibility(View.INVISIBLE);
                tvContent.setVisibility(View.INVISIBLE);
                btnEdit.setVisibility(View.INVISIBLE);
                btnPrev.setVisibility(View.INVISIBLE);

                editTitle.setVisibility(View.VISIBLE);
                editTitle.setText(tvTitle.getText());
                editContent.setVisibility(View.VISIBLE);
                editContent.setText(tvContent.getText());

                btnSet.setVisibility(View.VISIBLE);
                tvCancel.setVisibility(View.VISIBLE);

            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.add(R.id.journalList, new JournalListFragment());
                fragmentTransaction.commit();
            }
        });

        btnSet.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String s = editTitle.getEditableText().toString();
                tvTitle.setText(s);
                String s2 = editContent.getEditableText().toString();
                tvContent.setText(s2);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                tvTitle.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.VISIBLE);
                btnPrev.setVisibility(View.VISIBLE);
                tvContent.setVisibility(View.VISIBLE);

                editTitle.setVisibility(View.INVISIBLE);
                editContent.setVisibility(View.INVISIBLE);
                btnSet.setVisibility(View.INVISIBLE);
                tvCancel.setVisibility(View.INVISIBLE);

            }
        });

        return view;
    }


    public void sendGet() {
        api.get_Journal_retrofit(token,journal_id).enqueue(new Callback<JournalRepo>() {
            @Override
            public void onResponse(Call<JournalRepo> call, Response<JournalRepo> response) {
                String getCode = Integer.toString(response.code());
//                Log.d("journal",token);
//                Log.d("journal",String.valueOf(id));
//                Log.v("daaa", response.message().toString());
//                Log.v("aadf", response.headers().toString());
//                Log.v("qer", response.raw().toString());
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        try {
                            JournalRepo repo = response.body();
                            tvTitle.setText(repo.getSubject());
                            tvContent.setText(repo.getContent());
                            tvMaxt.setText(String.valueOf(repo.getWeather().getMax_temp()));
                            tvMint.setText(String.valueOf(repo.getWeather().getMin_temp()));
                            tvHumid.setText(String.valueOf(repo.getWeather().getDaily_humid()));

                            String[] year_month = repo.getDatetime().split("-");
                            String[] day = year_month[2].split("T");

                            tvYear.setText(year_month[0]);
                            tvMonthofyear.setText(year_month[1]);
                            tvDayofmonth.setText(day[0]);

                            int watercount = repo.getDetection().getWater_count();
                            tvWatercount.setText(String.valueOf(watercount));
                            tvRefreshcount.setText(String.valueOf(repo.getDetection().getRefresh_count()));


                            if(watercount==0) countSpace.setBackgroundColor(Color.RED);
                            else if(watercount==1) countSpace.setBackgroundColor(Color.YELLOW);
                            else countSpace.setBackgroundColor(Color.GREEN);

                            String status = repo.getWeather().getStatus();
                            if(status.equals("Snow")){
                                ivWeatherstatus.setImageResource(R.mipmap.w32);
                            } else if(status.equals("Clear")){
                                ivWeatherstatus.setImageResource(R.mipmap.w01);
                            } else if(status.equals("Clouds")){
                                ivWeatherstatus.setImageResource(R.mipmap.w03);
                            } else if(status.equals("Rain")){
                                ivWeatherstatus.setImageResource(R.mipmap.w21);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, getCode);
                        }
                    } else {
                        Log.d(TAG, getCode);
                    }
                }
            }

            @Override
            public void onFailure(Call<JournalRepo> call, Throwable t) {
                Log.d("jorunal",t.toString());
            }
        });
    }

    public void sendPatch(String nTitle, String nContent) {
        Map<String, Object> params = new ArrayMap<>();
        params.put("subject",nTitle);
        params.put("content",nContent);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8")
                ,(new JSONObject(params)).toString());

        api.set_Journal_retrofit(token,id,body).enqueue(new Callback<JournalRepo>() {
            @Override
            public void onResponse(Call<JournalRepo> call, Response<JournalRepo> response) {
                String getCode = Integer.toString(response.code());
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        try {
                            JournalRepo repo = response.body();
                            tvTitle.setText(repo.getSubject());
                            tvContent.setText(repo.getContent());

                            tvTitle.setVisibility(View.VISIBLE);
                            tvContent.setVisibility(View.VISIBLE);
                            btnEdit.setVisibility(View.VISIBLE);
                            btnPrev.setVisibility(View.VISIBLE);

                            editTitle.setVisibility(View.INVISIBLE);
                            editContent.setVisibility(View.INVISIBLE);
                            btnSet.setVisibility(View.INVISIBLE);
                            tvCancel.setVisibility(View.INVISIBLE);
                            Log.i(TAG, "patch submitted to API." + response.body().toString());


                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, getCode);
                        }
                    } else {
                        Log.d(TAG, getCode);
                        if (response.code() == 401) {
                            try {
                                Log.v("daaa", response.message().toString());
                                Log.v("aadf", response.headers().toString());
                                Log.v("qer", response.raw().toString());
                                Log.v("Error code 400", response.errorBody().string());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<JournalRepo> call, Throwable t) {
                Log.e(TAG, "Unable to submit patch to API.");
            }
        });

    }
}
