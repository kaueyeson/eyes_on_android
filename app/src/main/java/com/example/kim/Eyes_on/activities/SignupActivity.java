package com.example.kim.Eyes_on.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.kim.Eyes_on.retrofit.ApiUtils;
import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.apiInterfaces.UsersApiInterface;
import com.example.kim.Eyes_on.repositories.UsersRepo;

import org.json.JSONObject;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Bind;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Sangwoo Kim on 2017. 5. 12..
 */

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    private UsersApiInterface api;

    @Bind(R.id.input_firstname) EditText _firstnameText;
    @Bind(R.id.input_lastname) EditText _lastnameText;
    @Bind(R.id.input_email) EditText _emailText;
    @Bind(R.id.input_mobile1) Spinner _mobileText1;
    @Bind(R.id.input_mobile2) EditText _mobileText2;
    @Bind(R.id.input_mobile3) EditText _mobileText3;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @Bind(R.id.btn_register) Button _registerButton;
    @Bind(R.id.btn_login) Button _loginButton;
    @Bind(R.id.input_dob_year) EditText _dobyearText;
    @Bind(R.id.input_dob_month) EditText _dobmonthText;
    @Bind(R.id.input_dob_day) EditText _dobdayText;
    @Bind(R.id.input_gender) Spinner _gender;
    @Bind(R.id.input_job) EditText _jobText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        api = ApiUtils.getUsersAPIService();

        _registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 등록완료 후 로그인 페이지로 이동
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void onBackPressed() {
        // 뒤로가기 버튼 눌렀을 시, 로그인 페이지로 이동
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        //버튼 비활성화
        _registerButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("가입 처리중...");
        progressDialog.show();

        String firstname = _firstnameText.getText().toString();
        String lastname = _lastnameText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile1 = _mobileText1.getSelectedItem().toString();
        String mobile2 = _mobileText2.getText().toString();
        String mobile3 = _mobileText3.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();
        String mobile = mobile1 + mobile2 + mobile3;
        int dob_year = Integer.parseInt(_dobyearText.getText().toString());
        int dob_month = Integer.parseInt(_dobmonthText.getText().toString());
        int dob_day = Integer.parseInt(_dobdayText.getText().toString());
        String job = _jobText.getText().toString();
        String gender = _gender.getSelectedItem().toString();


        Map<String, Object> params = new ArrayMap<>();
        params.put("phone_number", mobile);
        params.put("password", password);
        params.put("email", email);
        params.put("first_name", firstname);
        params.put("last_name", lastname);
        params.put("dob_year", dob_year);
        params.put("dob_month", dob_month);
        params.put("dob_day", dob_day);
        params.put("job", job);
        params.put("gender", gender);


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8")
                , (new JSONObject(params)).toString());
        api.addUser(body).enqueue(new Callback<UsersRepo>() {
            @Override
            public void onResponse(Call<UsersRepo> call, Response<UsersRepo> response) {
                String getCode = Integer.toString(response.code());
                Log.d(TAG, getCode);
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        onSignupSuccess();
                                        progressDialog.dismiss();
                                    }
                                }, 1000);
                    }
                } else {
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    onSignupFailed();
                                    progressDialog.dismiss();
                                }
                            }, 1000);
                }
            }

            @Override
            public void onFailure(Call<UsersRepo> call, Throwable t) {
                Log.d(TAG, t.toString());
            }
        });
    }

    public void onSignupSuccess() {
        _registerButton.setEnabled(true);
        Intent mainIntent = new Intent(SignupActivity.this,LoginActivity.class);
        SignupActivity.this.startActivity(mainIntent);
        SignupActivity.this.finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.fail_to_signup), Toast.LENGTH_SHORT).show();

        _registerButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

//        String lastname = _lastnameText.getText().toString();
//        String firstname =_firstnameText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile2 = _mobileText2.getText().toString();
        String mobile3 = _mobileText3.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.error_invalid_email));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (mobile2.isEmpty() || mobile2.length()<3) {
            _mobileText2.setError(getString(R.string.error_invalid_phone));
            valid = false;
        } else {
            _mobileText2.setError(null);
        }

        if (mobile3.isEmpty() || mobile3.length()<3) {
            _mobileText3.setError(getString(R.string.error_invalid_phone));
            valid = false;
        } else {
            _mobileText3.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 ) {
            _passwordText.setError(getString(R.string.error_invalid_password));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError(getString(R.string.error_incorrect_password));
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }
}