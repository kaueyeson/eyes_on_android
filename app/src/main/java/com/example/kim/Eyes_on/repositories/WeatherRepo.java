package com.example.kim.Eyes_on.repositories;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sangwoo Kim on 2017. 4. 6..
 */

public class WeatherRepo {

    @SerializedName("result")
    Result result;
    @SerializedName("weather")
    weather weather;

    public class Result {
        @SerializedName("code") String code;
        public String getCode() {return code;}
    }

    public class weather {

        public List<minutely> minutely = new ArrayList<>();
        public List<minutely> getMinutely() {return minutely;}

        public class minutely {
            @SerializedName("sky") Sky sky;
            @SerializedName("temperature") temperature temperature;
            @SerializedName("wind") wind wind;

            @SerializedName("humidity") String humidity;
            public String getHumidity(){
                return humidity;
            }
            @SerializedName("timeObservation") String timeObservation;
            public String getTimeObservation(){
                return timeObservation;
            }

            public class Sky{
                @SerializedName("code") String code;
                public String getCode() {return code;}
            }
            public class temperature{
                @SerializedName("tc") String tc; // 현재 기온
                public String getTc() {return tc;}
            }

            public class wind{ // 바람
                @SerializedName("wspd") String wspd;
                public String getWspd() {return wspd;}
            }

            public minutely.Sky getSky() {return sky;}
            public minutely.temperature getTemperature() {return temperature;}
            public minutely.wind getWind() {return wind;}



        }

    }
    public Result getResult() {return result;}
    public weather getWeather() {return weather;}

}



