package com.example.kim.Eyes_on.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.kim.Eyes_on.retrofit.ApiUtils;
import com.example.kim.Eyes_on.R;
import com.example.kim.Eyes_on.activities.GraphActivity;
import com.example.kim.Eyes_on.activities.LoginActivity;
import com.example.kim.Eyes_on.adapters.ListViewAdapter;
import com.example.kim.Eyes_on.apiInterfaces.EyesApiInterface;
import com.example.kim.Eyes_on.repositories.EyesOnRepo;

import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by Sangwoo Kim on 2017. 6. 1..
 */

public class ConnectionFragment extends Fragment {
    private static final String TAG = "ConnectionFragment";
    static final ArrayList<String> Temp = new ArrayList<>();
    static final ArrayList<String> Humi = new ArrayList<>();
    static final ArrayList<String> TimeList = new ArrayList<>();
    static final ArrayList<String> WaterDetection = new ArrayList<>(); //임시로 만듬
    static final ArrayList<String> Soil = new ArrayList<>();

    ListViewAdapter adapter;
    private EyesApiInterface api;
    private String token;
    private int id;

    @Bind(R.id.listview) ListView listview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connection, container, false);

        ButterKnife.bind(this,view);
        api = ApiUtils.getEyesAPIService();
        token = LoginActivity.getToken();
        id = LoginActivity.getId();

        adapter = new ListViewAdapter();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // 각 비닐하우스 아이템 클릭 시, 해당 비닐하우스의 온습도 그래프 페이지로 화면전환
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(getActivity(),GraphActivity.class);
                intent.putExtra("msg", position);

                startActivity(intent);
            }
        }) ;

        getEyeson();

        return view;
    }

    public void getEyeson(){
        final String device_type="seed";

        Log.d("userToken",token);

        api.get_Eyeson_retrofit(token, device_type, 1).enqueue(new Callback<EyesOnRepo>() {
            @Override
            public void onResponse(Call<EyesOnRepo> call, Response<EyesOnRepo> response) {
                String getCode = Integer.toString(response.code());
                if (response.isSuccessful()) {
                    Log.d(TAG,getCode);
                    if (response.code() == 200) {
                        try {
                            EyesOnRepo repo = response.body();

                            String temp = String.valueOf(repo.getData().get(0).getSensor_value().getTemp());
                            String humid = String.valueOf(repo.getData().get(0).getSensor_value().getHumid());


                            adapter.addItem("seed_1",temp,humid);
                            listview.setAdapter(adapter);


                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, getCode);
                        }
                    }
                } else{
                    Log.d(TAG, getCode);
                }
            }
            @Override
            public void onFailure(Call<EyesOnRepo> call, Throwable t) {
                Log.d(TAG, t.toString() );
            }
        });

        api.get_Eyeson_retrofit(token, device_type, 2).enqueue(new Callback<EyesOnRepo>() {
            @Override
            public void onResponse(Call<EyesOnRepo> call, Response<EyesOnRepo> response) {
                String getCode = Integer.toString(response.code());
                if (response.isSuccessful()) {
                    Log.d(TAG,getCode);
                    if (response.code() == 200) {
                        try {
                            EyesOnRepo repo = response.body();

                            String temp = String.valueOf(repo.getData().get(0).getSensor_value().getTemp());
                            String humid = String.valueOf(repo.getData().get(0).getSensor_value().getHumid());

                            adapter.addItem("seed_2",temp,humid);
                            listview.setAdapter(adapter);


                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(TAG, getCode);
                        }
                    }
                } else{
                    Log.d(TAG, getCode);
                }
            }
            @Override
            public void onFailure(Call<EyesOnRepo> call, Throwable t) {
                Log.d(TAG, t.toString() );
            }
        });
    }

    public ArrayList<String> getTemp()
    {
        return Temp;
    }
    public ArrayList<String> getHumi()
    {
        return Humi;
    }
    public ArrayList<String> getSoil()
    {
        return Soil;
    }

    public static ArrayList<String> getTimeList() {
        return TimeList;
    }
    public ArrayList<String> getWaterDetection() {return WaterDetection;}//임시로 만듦
}
